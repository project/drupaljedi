DrupalJedi is a Drupal distribution which include contrib modules, which usage is a de facto standard, like:

Elysia cron
Metatag
Filefield paths
Module filter
Features
Entity API
Global redirect
Pathauto
Rules
Transliteration
Strongarm
Link
Nodequeue
Ckeditor
Email registration
Backup and Migrate

DrupalJedi structure architecture is based on panels + panels everywhere modules
Also nice admin theme provided which designed for non-experienced users and allow to easy manage content on the site.
Front theme uses bootstrap and configured specially for usage with panels.
